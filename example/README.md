Example playbook to test module.

Usage:

```
docker-compose up -d mssql
docker-compose run --rm ansible ansible-playbook example/tests/test.yml```
docker-compose stop mssql
```
