#!/usr/bin/python
# coding: utf-8

# (c) 2021, Benjamin Perche <b.perche@controlaltsoft.com>
# Outline and parts are reused from Vedit Firat Arig's <firatarig@gmail.com> mssql_db module
# (See https://github.com/ansible-collections/community.general/blob/main/plugins/modules/database/mssql/mssql_db.py)
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


DOCUMENTATION = '''
---
module: mssql_db_odbc
short_description: Add or remove MSSQL databases from a remote host using pyodbc driver.
description:
   - Add or remove MSSQL databases from a remote host using pyodbc driver.
options:
  name:
    description:
      - name of the database to add or remove
    required: true
    aliases: [ db ]
    type: str
  login_user:
    description:
      - The username used to authenticate with
    type: str
  login_password:
    description:
      - The password used to authenticate with
    type: str
  login_host:
    description:
      - Host running the database
    type: str
    required: true
  login_port:
    description:
      - Port of the MSSQL server. Requires login_host be defined as other than localhost if login_port is used
    default: '1433'
    type: str
  login_database:
    description:
      - The database name to start the connection with.
    default: 'master'
    type: str
  state:
    description:
      - The database state
    default: present
    choices: [ "present", "absent", "import" ]
    type: str
  target:
    description:
      - Location, on the remote host, of the dump file to read from or write to. Uncompressed SQL
        files (C(.sql)) files are supported.
    type: str
  autocommit:
    description:
      - Automatically commit the change only if the import succeed. Sometimes it is necessary to use autocommit=true, since some content can't be changed
        within a transaction.
    type: bool
    default: 'no'
  driver_name:
    description:
      - The odbc driver name. Default value depends on the platform.
    default: ''
    type: str
  mars_connection:
    description:
      - Whether or not to use MARS_Connection login property.
    type: bool
    default: 'yes'
notes:
   - Requires the pyodbc Python package on the remote host. For Ubuntu, this
     is as easy as pip install pyodbc (See M(ansible.builtin.pip).)
   - Requires an official MSSQL 17 ODBC driver (See https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver17) 
requirements:
   - python >= 2.7
   - pyodbc
author: Benjamin Perche
'''

EXAMPLES = '''
- name: Create a new database with name 'jackdata'
  mssql_db_odbc:
    name: jackdata
    state: present

# Copy database dump file to remote host and restore it to database 'my_db'
- name: Copy database dump file to remote host
  ansible.builtin.copy:
    src: dump.sql
    dest: /tmp

- name: Restore the dump file to database 'my_db'
  mssql_db_odbc:
    name: my_db
    state: import
    target: /tmp/dump.sql
'''

RETURN = '''
#
'''

import os
import traceback

PYODBC_IMP_ERR = None
try:
    import pyodbc
except ImportError:
    PYODBC_IMP_ERR = traceback.format_exc()
    mssql_found = False
else:
    mssql_found = True

from ansible.module_utils.basic import AnsibleModule, missing_required_lib


def get_platform_driver(driver_name):
    if not driver_name:
        if os.name == 'nt':
            # This code is generic and should work on any windows workstation
            driver_name = 'SQL Server'
        else:
            # This code is not generic and is intended to work on debian buster with official microsoft odbc driver
            driver_name = 'ODBC Driver 17 for SQL Server'

    if driver_name.startswith('{'):
        return driver_name

    return '{%s}' % driver_name


def db_exists(conn, cursor, db):
    cursor.execute("SELECT name FROM master.sys.databases WHERE name = ?", db)
    conn.commit()
    return bool(cursor.rowcount)


def db_create(conn, cursor, db):
    cursor.execute("CREATE DATABASE [%s]" % db)
    return db_exists(conn, cursor, db)


def db_delete(conn, cursor, db):
    try:
        cursor.execute("ALTER DATABASE [%s] SET single_user WITH ROLLBACK IMMEDIATE" % db)
    except Exception:
        pass
    cursor.execute("DROP DATABASE [%s]" % db)
    return not db_exists(conn, cursor, db)


def db_import(conn, cursor, module, db, target):
    if os.path.isfile(target):
        with open(target, 'r') as backup:
            sql_query = "USE [%s]\n" % db
            for line in backup:
                if line is None:
                    break
                elif line.startswith('GO'):
                    cursor.execute(sql_query)
                    sql_query = "USE [%s]\n" % db
                else:
                    sql_query += line
            cursor.execute(sql_query)
            conn.commit()
        return 0, "import successful", ""
    else:
        return 1, "cannot find target file", "cannot find target file"


def main():
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(required=True, aliases=['db']),
            login_user=dict(default=''),
            login_password=dict(default='', no_log=True),
            login_host=dict(required=True),
            login_port=dict(default='1433'),
            login_database=dict(default='master'),
            driver_name=dict(default=''),
            mars_connection=dict(type='bool', default=True),
            target=dict(default=None),
            autocommit=dict(type='bool', default=False),
            state=dict(
                default='present', choices=['present', 'absent', 'import'])
        )
    )

    if not mssql_found:
        module.fail_json(msg=missing_required_lib('pyodbc'), exception=PYODBC_IMP_ERR)

    db = module.params['name']
    state = module.params['state']
    autocommit = module.params['autocommit']
    target = module.params["target"]

    login_user = module.params['login_user']
    login_password = module.params['login_password']
    login_host = module.params['login_host']
    login_port = module.params['login_port']
    login_database = module.params['login_database']
    driver_name = module.params['driver_name']
    mars_connection = module.params['mars_connection']

    login_querystring = login_host
    if login_port != "1433":
        login_querystring = "%s:%s" % (login_host, login_port)

    if login_user != "" and login_password == "":
        module.fail_json(msg="when supplying login_user arguments login_password must be provided")

    parts = {
        'DRIVER': get_platform_driver(driver_name),
        'SERVER': login_querystring,
        'UID': login_user,
        'PWD': login_password,
        'DATABASE': login_database,
    }

    if mars_connection:
        parts['MARS_Connection'] = 'Yes'

    try:
        conn = pyodbc.connect(';'.join('='.join([k, v]) for k, v in parts.items()))
        cursor = conn.cursor()
    except Exception as e:
        if "Unknown database" in str(e):
            errno, errstr = e.args
            module.fail_json(msg="ERROR: %s %s" % (errno, errstr))
        else:
            module.fail_json(msg="unable to connect, check login_user and login_password are correct, or alternatively check your "
                                 "@sysconfdir@/freetds.conf / ${HOME}/.freetds.conf : %s" % str(e))

    conn.autocommit = True
    changed = False

    if db_exists(conn, cursor, db):
        if state == "absent":
            try:
                changed = db_delete(conn, cursor, db)
            except Exception as e:
                module.fail_json(msg="error deleting database: " + str(e))
        elif state == "import":
            conn.autocommit = autocommit
            rc, stdout, stderr = db_import(conn, cursor, module, db, target)

            if rc != 0:
                module.fail_json(msg="%s" % stderr)
            else:
                module.exit_json(changed=True, db=db, msg=stdout)
    else:
        if state == "present":
            try:
                changed = db_create(conn, cursor, db)
            except Exception as e:
                module.fail_json(msg="error creating database: " + str(e))
        elif state == "import":
            try:
                changed = db_create(conn, cursor, db)
            except Exception as e:
                module.fail_json(msg="error creating database: " + str(e))

            conn.autocommit = autocommit
            rc, stdout, stderr = db_import(conn, cursor, module, db, target)

            if rc != 0:
                module.fail_json(msg="%s" % stderr)
            else:
                module.exit_json(changed=True, db=db, msg=stdout)

    module.exit_json(changed=changed, db=db)


if __name__ == '__main__':
    main()
