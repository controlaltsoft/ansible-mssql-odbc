Ansible MSSQL ODBC
===

This ansible module is a port of `mssql_db` module using `pyodbc` driver instead of `pymssql` to connect to the database.

Install
---

In order to make this module work, you will need to install:
- An official [ODBC driver from microsoft](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver17)
- `pyodbc` package using pip.

Then you need either to copy `mssql_db_odbc.py` in `~/.ansible/plugins/modules` (or any of the path listed in `configured module search path` from `ansible --version`) or add the path you cloned this repository in to the `ANSIBLE_LIBRARY` environment variable.

Usage
---

You may add any task using this module to create / delete a database or import a file

```
- name: Init database
  mssql_db_odbc:
    name: testdb
    state: present
    login_host: mssql
    login_port: 1433
    login_user: sa
    login_password: Passw0rd!
```

Author
---

Benjamin Perche <b.perche@controlaltsoft.com>

License
---

GPL-v3+ (See [COPYING](COPYING))
